import React from "react";

const Search = () => {
    return <div className={'w-100 d-flex mt-3'}>
        <input type="text"
               className={'search'}
               placeholder={'Поиск по ключевому слову'} />
        <button className={'search'}>Поиск</button>
    </div>
}

export default Search;