class HttpService {
    private defaultHeaders: any = {};

    setDefaultHeader(name: string, value: any): void {
        this.defaultHeaders[name] = value;
    }

    removeDefaultHeader(name: string): void {
        delete this.defaultHeaders[name];
    }

    get(url: string, /*data: any = {}, */headers: any = {}): Promise<Response> {
        return this.send({
            method: "GET",
            url,
            headers,
            // body: JSON.stringify(data),
        });
    }

    post(url: string, data: any = {}, headers: any = {}): Promise<Response> {
        return this.send({
            method: "POST",
            url,
            headers,
            body: JSON.stringify(data),
        });
    }

    put(url: string, data: any = {}, headers: any = {}): Promise<Response> {
        return this.send({
            method: "PUT",
            url,
            headers,
            body: JSON.stringify(data),
        });
    }

    delete(url: string, data: any = {}, headers: any = {}): Promise<Response> {
        return this.send({
            method: "DELETE",
            url,
            headers,
            body: JSON.stringify(data),
        });
    }

    patch(url: string, data: any = {}, headers: any = {}): Promise<Response> {
        return this.send({
            method: "PATCH",
            url,
            headers,
            body: JSON.stringify(data),
        });
    }

    send(requestInfo: any): Promise<Response> {
        return this.sendFetch(requestInfo, false, true);
    }

    withFormData(method: string, url: string, data: FormData = null, headers: any = {}): Promise<Response> {
        const options = {
            method,
            url,
            headers,
            body: data,
        };

        return this.sendFetch(options, true, true);
    }

    otherSite(url: string): Promise<Response> {
        const options = {
            method: "GET",
            url,
        };

        return this.sendFetch(options, true, false);
    }

    getDomain(): string {
        return process.env.REACT_APP_API_ENDPOINT;
    }

    getApiDomain() {
        return this.getDomain() + '/api/';
    }

    private sendFetch(requestInfo: any, isFormData: boolean, isSame: boolean): Promise<Response> {
        const {url} = requestInfo;
        delete requestInfo.url;

        requestInfo.headers = {
            // "Accept": "application/json",
            "X-Request-With": "XMLHttpRequest",
            ...this.defaultHeaders,
            ...requestInfo.headers,
        };

        if (!isFormData) {
            requestInfo.headers["Content-Type"] = "application/json";
        }

        requestInfo.redirect = "follow";

        const reqUrl = url[0] == "/" ? url.substr(1) : url;
        return fetch(`${isSame ? this.getApiDomain() : ""}${reqUrl}`, requestInfo);
    }
}

export default new HttpService();
