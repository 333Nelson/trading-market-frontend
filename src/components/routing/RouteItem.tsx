import React from "react";
import {Route, Redirect} from "react-router-dom";
import {connect} from "react-redux";

import Router from "./Router";

const makeRouteViewWithoutLayout = (args: any, ViewComponent: any) => {
    return <Route {...args} render={(props: any) => (
        <ViewComponent {...props} />
    )}/>;
};

const makeRouteViewWithLayout = (args: any, ViewComponent: any) => {
    const PageLayout = args.layout;
    return <Route {...args} render={(props: any) => (
        <PageLayout>
            <ViewComponent {...props} />
        </PageLayout>
    )}/>;
};

const makeRouteRedirectToLogin = (args: any) => {
    const redirectData = {
        pathname: Router.toUrl("account_login"),
        state: {
            from: args.location,
        },
    };

    return <Redirect to={redirectData}/>;
};

const RouteItem = ({component, isLogged, loggedUser, ...args}: any) => {
    if (!args.auth || (args.auth && isLogged)) {
        if (args.layout === null || args.layout === undefined) {
            return makeRouteViewWithoutLayout(args, component);
        } else {
            return makeRouteViewWithLayout(args, component);
        }
    } else {
        return makeRouteRedirectToLogin(args);
    }
};

export default connect((state: any) => {
    return {
        isLogged: state.AuthReducer.isLogged,
        loggedUser: state.AuthReducer.user,
    }
})(RouteItem);
