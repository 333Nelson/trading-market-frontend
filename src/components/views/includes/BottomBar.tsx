import React from "react";

const BottomBar = () => {
    return <div className={'bottom-bar'}>
        <div>
            <h5>Телефон тех. поддержки</h5>
            <h6 className={'mt-2'}>8-800-200-0161</h6>
            <a href="/">info@рынок-торгов.рф</a>
            <p className={'mt-2'}>По будням с 9:00 до 18:00 (мск)</p>
        </div>
        <div>
            <h5>Информация</h5>

            <ul className={'p-0 m-0'}>
                <li><a href="/">Обратная связь</a></li>
                <li><a href="/">Информация об оплате банковской картой</a></li>
                <li><a href="/">Политика конфиденциальности</a></li>
                <li><a href="/">Пользовательское соглашение</a></li>
            </ul>
        </div>
    </div>
}

export default BottomBar;