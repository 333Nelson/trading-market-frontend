import React from "react";
import { Nav, Navbar, NavItem } from "reactstrap";
import {Link} from "react-router-dom";
import Route from "../../routing/Router";

const SubMenu = () => {
    return <header>
        <Navbar className={'d-flex justify-content-center sub-menu'} expand="md">
            <Nav className="me-auto" navbar>
                <NavItem>
                    <Link className="nav-link" to={Route.toUrl("home")}>Торги</Link>
                </NavItem>
                <NavItem>
                    <Link className="nav-link" to={Route.toUrl("home")}>Избранное</Link>
                </NavItem>
                <NavItem>
                    <Link className="nav-link" to={Route.toUrl("home")}>Мониторинг</Link>
                </NavItem>
                <NavItem>
                    <Link className="nav-link" to={Route.toUrl("home")}>Успей купить</Link>
                </NavItem>
                <NavItem>
                    <Link className="nav-link" to={Route.toUrl("home")}>Продажи</Link>
                </NavItem>
                <NavItem>
                    <Link className="nav-link" to={Route.toUrl("home")}>Реестры</Link>
                </NavItem>
            </Nav>
        </Navbar>
    </header>
}

export default SubMenu;