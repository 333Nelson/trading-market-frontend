import React from 'react';
import IMG1 from '../../../../assets/images/2.jpg';
import IMG2 from '../../../../assets/images/3.jpg';
import IMG3 from '../../../../assets/images/4.jpg';
import IMG4 from '../../../../assets/images/5.jpg';
import IMG5 from '../../../../assets/images/6.jpg';
import IMG6 from '../../../../assets/images/7.jpg';
import IMG7 from '../../../../assets/images/8.jpg';
import IMG8 from '../../../../assets/images/9.jpg';

const Categories = () => {
    return <div className={'categories-container'}>
        <h3>Выбрать категорию</h3>
        <div>
            <ul>
                <li>
                    <img src={IMG1} alt="IMG1"/>
                </li>
                <li>
                    <img src={IMG2} alt="IMG2"/>
                </li>
                <li>
                    <img src={IMG3} alt="IMG3"/>
                </li>
                <li>
                    <img src={IMG4} alt="IMG4"/>
                </li>
            </ul>
            <ul>
                <li>
                    <img src={IMG5} alt="IMG5"/>
                </li>
                <li>
                    <img src={IMG6} alt="IMG6"/>
                </li>
                <li>
                    <img src={IMG7} alt="IMG7"/>
                </li>
                <li>
                    <img src={IMG8} alt="IMG8"/>
                </li>
            </ul>

            <div>
                <input type="checkbox" id={'choose-all'}/>
                <label htmlFor="choose-all">Выбрать все</label>
            </div>
            <div>
                <input type="checkbox" id={'light-transport'}/>
                <label htmlFor="light-transport">Легковой транспорт</label>
            </div>
            <div>
                <input type="checkbox" id={'freight-and-comm-transport'}/>
                <label htmlFor="freight-and-comm-transport">Грузовой и комм. транспорт</label>
            </div>
            <div>
                <input type="checkbox" id={'special-equipment'}/>
                <label htmlFor="special-equipment">Спецтехника</label>
            </div>
            <div>
                <input type="checkbox" id={'buses-minibuses'}/>
                <label htmlFor="buses-minibuses">Автобусы, микроавтобусы</label>
            </div>
            <div>
                <input type="checkbox" id={'water-transport'}/>
                <label htmlFor="water-transport">Водный транспорт</label>
            </div>
            <div>
                <input type="checkbox" id={'residential-properties'}/>
                <label htmlFor="residential-properties">Жилая недвижимость</label>
            </div>
            <div>
                <input type="checkbox" id={'comm-the-property'}/>
                <label htmlFor="comm-the-property">Комм. недвижимость</label>
            </div>
            <div>
                <input type="checkbox" id={'land'}/>
                <label htmlFor="land">Земельные участки</label>
            </div>
            <div>
                <input type="checkbox" id={'garages-buildings-structures'}/>
                <label htmlFor="garages-buildings-structures">Гаражи, строения, сооружения</label>
            </div>
            <div>
                <input type="checkbox" id={'debt-of-legal-entities-persons'}/>
                <label htmlFor="debt-of-legal-entities-persons">Задолженность юр. лиц</label>
            </div>
            <div>
                <input type="checkbox" id={'agricultural-buildings-and-structures'}/>
                <label htmlFor="agricultural-buildings-and-structures">С/х здания и соружения</label>
            </div>
            <div>
                <input type="checkbox" id={'agricultural-complex'}/>
                <label htmlFor="agricultural-complex">С/х комплекс</label>
            </div>
            <div>
                <input type="checkbox" id={'agricultural-equipment'}/>
                <label htmlFor="agricultural-equipment">С/х оборудование</label>
            </div>
            <div>
                <input type="checkbox" id={'prom-equipment'}/>
                <label htmlFor="prom-equipment">Пром. оборудование</label>
            </div>
            <div>
                <input type="checkbox" id={'woodworking'}/>
                <label htmlFor="woodworking">Деревообработка</label>
            </div>
            <div>
                <input type="checkbox" id={'food-equipment'}/>
                <label htmlFor="food-equipment">Пищевое оборудование</label>
            </div>
            <div>
                <input type="checkbox" id={'construction-equipment'}/>
                <label htmlFor="construction-equipment">Строительное оборудование</label>
            </div>
            <div>
                <input type="checkbox" id={'other-equipment'}/>
                <label htmlFor="other-equipment">Строительное оборудование</label>
            </div>
            <div>
                <input type="checkbox" id={'furniture'}/>
                <label htmlFor="furniture">Мебель</label>
            </div>
            <div>
                <input type="checkbox" id={'appliances'}/>
                <label htmlFor="appliances">Бытовая техника</label>
            </div>
            <div>
                <input type="checkbox" id={'precious-metals-precious-metals-stones-and-products-from-them'}/>
                <label htmlFor="precious-metals-precious-metals-stones-and-products-from-them">Драгоценные металлы, драг. камни и изделия из них</label>
            </div>
        </div>
    </div>
}

export default Categories;