import React, {useEffect} from "react";
import {HashRouter, Switch, Redirect} from "react-router-dom";
import RouteItem from "./routing/RouteItem";
import Routes from "./Routes";
import store from "./store";
import { AUTH_CHECK } from "./store/actions";

const App = () => {

    useEffect(() => {
        store.dispatch({
            type: AUTH_CHECK,
        });
    }, [])

    return <div>
        <HashRouter>
            <Switch>
                {Routes.redirectList().map((redirect, i) => (
                    <Redirect key={i} {...redirect} />
                ))}

                {Routes.routeList().map((route, i) => (
                    <RouteItem key={i} {...route} />
                ))}
            </Switch>
        </HashRouter>
    </div>;
}

export default App
