import React from 'react'
import {Button} from "reactstrap";

const MainSettings = () => {
    return <div className={'main-settings-container'}>
        <div className={'mb-3'}>
            <label htmlFor="exclusion-words">Слова исключения</label>
            <input type="text" id={'exclusion-words'}/>
        </div>
        <div className={'mb-3'}>
            <span>Регион</span>
            <Button color="primary">Все Регионы</Button>
        </div>
        <div className={'mb-3'}>
            <span>Торговая Площадка</span>
            <Button color="primary">Все торговые площадки</Button>
        </div>
        <div className={'mb-3'}>
            <label htmlFor="trade-number">Номер торгов</label>
            <input type="text" id={'trade-number'}/>
        </div>
        <div className={'mb-3'}>
            <span>Типы торгов</span>
            <Button color="primary mr-4">Аукцион</Button>
            <Button color="primary">Публичное предложение</Button>
        </div>
        <div className={'mb-3'}>
            <label htmlFor="debtor-category">Категория должника</label>
            <select id={'debtor-category'}>
                <option selected value="1">Все Категории</option>
                <option value="2">Градообразуюшая организация</option>
            </select>
        </div>
        <div className={'mb-3'}>
            <input type="text" placeholder={'Должник'}/>
        </div>
        <div className={'mb-3'}>
            <input type="text" placeholder={'Арбитражный управляюший'}/>
        </div>
        <div className={'mb-3'}>
            <input type="text" placeholder={'Организатор торгов'}/>
        </div>
    </div>
}

export default MainSettings;