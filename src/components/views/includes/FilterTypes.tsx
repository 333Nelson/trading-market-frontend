import React, {useState} from "react";

const FilterTypes = (props) => {

    const handleClickTradingDate = () => {

    }

    const handleClickSearchPatterns = () => {

    }

    return <div className={'filter-types-container mt-3'}>
        <div className={'row'}>
            <div className={'item'} onClick={() => props.setMainSettings()}>
                <span className={'mr-2'}>Основные параметры</span>
                <span>
                    { props.mainSettings ? "-" : "+" }
                </span>
            </div>
            <div className={'item'} onClick={props.setCategories}>
                <span className={'mr-2'}>Категории</span>
                <span>
                    { props.categories ? "-" : "+" }
                </span>
            </div>
            <div className={'item'} onClick={() => props.setOtherParameters()}>
                <span className={'mr-2'}>Другие параметры</span>
                <span>
                    { props.otherParameters ? "-" : "+" }
                </span>
            </div>
        </div>
        <div className={'row'}>
            <div className={'item'} onClick={() => props.setValues()}>
                <span className={'mr-2'}>Стоимость</span>
                <span>
                    { props.values ? "-" : "+" }
                </span>
            </div>
            <div className={'item'} onClick={() => props.setTradingDate()}>
                <span className={'mr-2'}>Дата торгов</span>
                <span>
                    { props.tradingDate ? "-" : "+" }
                </span>
            </div>
            <div className={'item'} onClick={() => props.setSearchPatterns()}>
                <span className={'mr-2'}>Шаблоны поиска</span>
                <span>
                    { props.searchPatterns ? "-" : "+" }
                </span>
            </div>
        </div>
    </div>
}

export default FilterTypes;