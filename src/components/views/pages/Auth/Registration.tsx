import React, {useEffect, useState} from 'react';
import {Form, FormGroup, Label, Input, Row, Col} from 'reactstrap';
import http from "../../../services/HttpService";
import {
    useParams
} from "react-router-dom";

const Registration = () => {
    const { token } : any = useParams();
    const [id, setId] = useState(null);
    const [name, setName] = useState("");
    const [phone, setPhone] = useState("");
    const [email, setEmail] = useState("");

    useEffect(() => {
        http.get(`/reg/user/${token}`)
            .then(response => response.json())
            .then((data) => {
                setId(data.id);
                setName(data.name);
                setPhone(data.phone);
                setEmail(data.email);
                console.log(data);
            });
    }, []);

    const handleClick = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("id", id);
        formData.append("password", e.target.password.value);

        http.withFormData('post', '/reg/auth', formData)
            .then(response => response.json())
            .then((data) => {
                console.log(data);
            });
    }

    return (
        <div className={"registration"}>
            <Row>
                <Col md={12} sm={12}>
                    <Form onSubmit={(e) => handleClick(e)}>
                        <Row form>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for={"name"}>Անուն Ազգանուն</Label>
                                    <Input type={"text"}
                                           id={"name"}
                                           readOnly={true}
                                           value={name}
                                           name={"name"} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row form>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for={"phone"}>Հեռախոսահամար</Label>
                                    <Input type={"text"}
                                           id={"phone"}
                                           readOnly={true}
                                           value={phone}
                                           name={"phone"} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row form>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for={"email"}>Էլ․ Փոստ</Label>
                                    <Input type={"text"}
                                           id={"email"}
                                           readOnly={true}
                                           value={email}
                                           name={"email"} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row form>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="password">Գաղտնաբառ</Label>
                                    <Input
                                        type="password"
                                        id="password"
                                        name={"password"}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <div>
                            <FormGroup>
                                <Input type={"submit"} value={"Գրանցվել"} />
                            </FormGroup>
                        </div>
                    </Form>
                </Col>
            </Row>
        </div>
    );
};

export default Registration;