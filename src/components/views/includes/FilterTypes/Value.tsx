import React from 'react'

const Value = () => {
    return <div className={'values-container'}>
        <h3>Стоимость</h3>

        <div>
            <label htmlFor="starting-price">Цена начальная</label>
            <input type="text" id={'starting-price'} placeholder={'от'}/>
            -
            <input type="text" placeholder={'до'}/>
        </div>

        <div>
            <label htmlFor="current-price">Цена текущая</label>
            <input type="text" id={'current-price'} placeholder={'от'}/>
            -
            <input type="text" placeholder={'до'}/>
        </div>

        <div>
            <label htmlFor="minimum-price">Цена минимальная</label>
            <input type="text" id={'minimum-price'} placeholder={'от'}/>
            -
            <input type="text" placeholder={'до'}/>
        </div>

        <div>
            <label htmlFor="reduction-percentage">Процент снижения</label>
            <input type="text" id={'reduction-percentage'} placeholder={'от'}/>
            -
            <input type="text" placeholder={'до'}/>
        </div>
    </div>
}

export default Value;