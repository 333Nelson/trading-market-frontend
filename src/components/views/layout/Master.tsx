import React, {useState} from "react";
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
} from 'reactstrap';
import {Link} from "react-router-dom";
import Route from "../../routing/Router";
import { connect } from "react-redux";
import SubMenu from "../includes/SubMenu";
import BottomBar from "../includes/BottomBar";

const Master = (props) => {
    const [isOpen, setIsOpen] = useState(false);

    const toggle = () => setIsOpen(!isOpen);

    return (
        <>
            <header>
                <Navbar light className={'menu'} expand="md">
                    <NavbarBrand className={'logo'} href="/">рынок-торгов.рф</NavbarBrand>
                    <NavbarToggler onClick={toggle}/>
                    <Collapse isOpen={isOpen} navbar>
                        <Nav className="me-auto" navbar>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("home")}>Главная</Link>
                            </NavItem>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("about_us")}>О проекте</Link>
                            </NavItem>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("contact_as")}>Контакты</Link>
                            </NavItem>
                            {
                                !props.isLogged &&
                                    <>
                                        <NavItem>
                                            <Link className="nav-link" to={Route.toUrl("account_login")}>Вход</Link>
                                        </NavItem>
                                        <NavItem>
                                            <Link className="nav-link" to={Route.toUrl("account_register")}>Регистрация</Link>
                                        </NavItem>
                                    </>
                            }
                        </Nav>
                    </Collapse>
                </Navbar>
            </header>

            <SubMenu />

            <div className={"container"}>
                {props.children}
            </div>

            <BottomBar />

            <footer>
                <div>
                    <Navbar className={'d-flex justify-content-center footer-menu'} expand="md">
                        <Nav className="me-auto" navbar>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("home")}>Главное</Link>
                            </NavItem>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("about_us")}>О проекте</Link>
                            </NavItem>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("contact_as")}>Контакты</Link>
                            </NavItem>
                            <NavItem>
                                <Link className="nav-link" to={Route.toUrl("account_login")}>Вход</Link>
                            </NavItem>
                        </Nav>
                    </Navbar>
                </div>
                <div>
                    <p>Copyright © 2021, рынок-торгов.рф все права защищены</p>
                </div>
            </footer>
        </>
    )
}

const mapState = (state) => {
    return {
        isLogged: state.AuthReducer.isLogged,
    };
};

export default connect(mapState)(Master)