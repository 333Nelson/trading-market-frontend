import Route from "./routing/Router";

import HomePage from "./views/pages/Site/HomePage";
import NotFound from "./views/pages/NotFound";
import LayoutMaster from "./views/layout/Master";
import LayoutDashboard from "./views/layout/DashboardMaster";

import Login from "./views/pages/Auth/Login";
import Logout from "./views/pages/Auth/Logout";
import Register from "./views/pages/Auth/Registration";
// import VerifyAccount from "./views/pages/Auth/VerifyAccount";
// import ForgotPassword from "./views/pages/Auth/ForgotPassword";
// import ResetPassword from "./views/pages/Auth/ResetPassword";

import DashboardHome from "./views/pages/Dashboard/Home";

import AboutUs from "./views/pages/Site/AboutUs";
import ContactAs from "./views/pages/Site/ContactAs";

/* --------------------  account  --------------------- */
Route.group("/account", () => {
    Route.add("account_login", {
        path: "/login",
        component: Login,
        layout: LayoutMaster
    });

    Route.add("account_register", {
        path: "/register",
        component: Register,
        layout: LayoutMaster
    });

    Route.add("account_logout", {
        path: "/logout",
        component: Logout,
        //layout: AuthLayout
    });

    /*Route.add("account_forgot", {
        path: "/forgot-password",
        component: ForgotPassword,
        //layout: AuthLayout
    });

    Route.add("account_reset", {
        path: "/reset-password/:token",
        component: ResetPassword,
        //layout: AuthLayout
    });*/
});


/* --------------------  dashboard  --------------------- */
Route.group("/dashboard", () => {
    Route.redirectRoute("/", "dashboard_home");

    Route.add("dashboard_my_account_page", {
        path: "/my-account-page",
        component: DashboardHome,
        layout: LayoutDashboard,
        auth: true,
    });
});


/* --------------------  site URL  --------------------- */
Route.add("about_us", {
    path: "/about-us",
    component: AboutUs,
    layout: LayoutMaster,
});

Route.add("contact_as", {
    path: "/contact-us",
    component: ContactAs,
    layout: LayoutMaster,
});

Route.homePage(HomePage, LayoutMaster);
Route.error404(NotFound);


export default Route;