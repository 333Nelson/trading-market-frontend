import React, {useEffect} from 'react';
import {Form, FormGroup, Label, Input, Row, Col} from 'reactstrap';
import http from "../../../services/HttpService";
import { AUTH_LOGIN } from '../../../store/actions';
import {connect, useDispatch} from "react-redux";
import Route from "../../../routing/Router";
import { Redirect, useHistory } from "react-router-dom";

const Login = (props) => {
    const dispatch = useDispatch();
    const history = useHistory();

    useEffect(() => {
        if (props.isLogged) {
            history.push('/dashboard/my-account-page')
        }
    }, [props.isLogged])

    /*const handleClick = (e) => {
        e.preventDefault();
        const formData = new FormData();
        formData.append("email", e.target.email.value);
        formData.append("password", e.target.password.value);

        http.withFormData('post', '/auth', formData)
            .then(response => response.json())
            .then((data) => {
                dispatch({
                    type: AUTH_LOGIN,
                    payload: data,
                });
            });
    }*/

    const handleClick = (e) => {
        e.preventDefault();

        const newData = {
            access_token: "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJhZG1pbkBlbWFpbC5jb20iLCJleHAiOjE2MTgwNjE5MDYsImlhdCI6MTYxODA0MzkwNn0.Td0AVkXAwBBKehpZ3wQaCzHY_9XOSGqq4mL6IBB8IPMECgki28AuiTWu1llPEcx6Kmhpnw9wj1Qgvq4ROrmp1Q",
            user: {
                id: 1,
                name: "Name Surname",
                email: "0101@mail.com",
                phone: "099999999",
                roles: [
                    {
                        name: "admin",
                        permissions: [
                            {name: 'courses', is_view: 1, is_add: 1, is_edit: 1, is_delete: 1},
                            {name: 'users', is_view: 1, is_add: 1, is_edit: 1, is_delete: 1},
                        ],
                    },
                    {
                        name: "student",
                        permissions: [
                            {name: 'courses', is_view: 1, is_add: 0, is_edit: 0, is_delete: 0},
                            {name: 'users', is_view: 0, is_add: 0, is_edit: 0, is_delete: 0},
                        ],
                    },
                ]
            }
        }

        setTimeout(() => {
            dispatch({
                type: AUTH_LOGIN,
                payload: newData,
            });
        }, 2000)
    }

    return (
        <div className={"login"}>
            <Row>
                <Col md={12} sm={12}>
                    <Form onSubmit={(e) => handleClick(e)}>
                        <Row form>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for={"email"}>Էլ․ Փոստ</Label>
                                    <Input type={"text"} id={"email"} name={"email"} />
                                </FormGroup>
                            </Col>
                        </Row>
                        <Row form>
                            <Col md={12}>
                                <FormGroup>
                                    <Label for="password">Գաղտնաբառ</Label>
                                    <Input
                                        type="password"
                                        id="password"
                                        name={"password"}
                                    />
                                </FormGroup>
                            </Col>
                        </Row>
                        <div>
                            <FormGroup>
                                <Input type={"submit"} value={"Մուտք"} />
                            </FormGroup>
                        </div>
                    </Form>
                </Col>
            </Row>
        </div>
    );
};

const mapState = (state) => {
    return {
        isLogged: state.AuthReducer.isLogged,
    };
};

export default connect(mapState)(Login);