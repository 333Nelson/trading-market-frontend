import React from "react";
import Filter from "../../includes/Filter";
import Results from "../../includes/Results";

const HomePage = () => {

    return (
        <>
            <Filter />
            <Results />
        </>
    );
}

export default HomePage;