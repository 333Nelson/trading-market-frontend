import React from 'react';
import Img from '../../../assets/images/1.jpg';
import Info from '../../../assets/images/info.ico';
import Region from '../../../assets/images/target.ico';
import Calendar from '../../../assets/images/calendar.ico';
import Raise from '../../../assets/images/raise.ico';

const results = [
    {id: 1, name: "Недвижимость", image: "noImage.png", description: "Индивидуальный жилой дом, общей площадью 105,7 кв.м., расположенный по адресу: Республика Алтай, Кош-Агачский район, с. Кош-Агач, ул. Ключевая, 1А, кадастровый номер 04:10:040101:1904; земельный участок, общей площадью 792 кв.м., расположенный по адресу: Республика Алтай, Кош-Агачский район, с. Кош-Агач, ул. Ключевая, 1А, кадастровый номер 04:10:040101:802", region: "", dateOfTheAuction: "29.03.2021 00:00", startingPrice: 579912.48},
    {id: 2, name: "Недвижимость", image: "noImage.png", description: "Индивидуальный жилой дом, общей площадью 105,7 кв.м., расположенный по адресу: Республика Алтай, Кош-Агачский район, с. Кош-Агач, ул. Ключевая, 1А, кадастровый номер 04:10:040101:1904; земельный участок, общей площадью 792 кв.м., расположенный по адресу: Республика Алтай, Кош-Агачский район, с. Кош-Агач, ул. Ключевая, 1А, кадастровый номер 04:10:040101:802", region: "", dateOfTheAuction: "29.03.2021 00:00", startingPrice: 579912.48},
    {id: 3, name: "Недвижимость", image: "noImage.png", description: "Индивидуальный жилой дом, общей площадью 105,7 кв.м., расположенный по адресу: Республика Алтай, Кош-Агачский район, с. Кош-Агач, ул. Ключевая, 1А, кадастровый номер 04:10:040101:1904; земельный участок, общей площадью 792 кв.м., расположенный по адресу: Республика Алтай, Кош-Агачский район, с. Кош-Агач, ул. Ключевая, 1А, кадастровый номер 04:10:040101:802", region: "", dateOfTheAuction: "29.03.2021 00:00", startingPrice: 579912.48},
    {id: 4, name: "Недвижимость", image: "noImage.png", description: "Индивидуальный жилой дом, общей площадью 105,7 кв.м., расположенный по адресу: Республика Алтай, Кош-Агачский район, с. Кош-Агач, ул. Ключевая, 1А, кадастровый номер 04:10:040101:1904; земельный участок, общей площадью 792 кв.м., расположенный по адресу: Республика Алтай, Кош-Агачский район, с. Кош-Агач, ул. Ключевая, 1А, кадастровый номер 04:10:040101:802", region: "", dateOfTheAuction: "29.03.2021 00:00", startingPrice: 579912.48},
    {id: 5, name: "Недвижимость", image: "noImage.png", description: "Индивидуальный жилой дом, общей площадью 105,7 кв.м., расположенный по адресу: Республика Алтай, Кош-Агачский район, с. Кош-Агач, ул. Ключевая, 1А, кадастровый номер 04:10:040101:1904; земельный участок, общей площадью 792 кв.м., расположенный по адресу: Республика Алтай, Кош-Агачский район, с. Кош-Агач, ул. Ключевая, 1А, кадастровый номер 04:10:040101:802", region: "", dateOfTheAuction: "29.03.2021 00:00", startingPrice: 579912.48},
];

const Results = () => {
    return <div className={'auctions-container mt-3'}>
        {
            results.length > 0 ?
                results.map((item, index) => {
                    return <div className={'item'} key={index}>
                        <div className={'header'}>
                            <div>
                                <ul>
                                    <li>
                                        <a href="#">
                                            <img src={Info} alt={'Info'} className={"mr-2"} />
                                            Лот {index + 1}
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            ЭТП
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Купить
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div>
                                <span>
                                    <img src={Region} alt="Region" className={'mr-2'}/>
                                    Регион
                                </span>
                                <span>
                                    <img src={Calendar} alt="Calendar" className={'mr-2'} />
                                    Дата проведения аукциона: {item.dateOfTheAuction}
                                </span>
                            </div>
                        </div>

                        <div className={'content'}>
                            <div>
                                <img src={Img} alt="Image" style={{width: 225, height: 130}}/>
                            </div>
                            <div>
                                <h5>{item.name}</h5>
                                <p>{item.description}</p>
                            </div>
                            <div>
                                <h6>Начальная цена</h6>
                                <div className={'d-flex align-items-center'}>
                                    <img src={Raise} alt="Raise"/>
                                    <p className={'m-0'}>{item.startingPrice}</p>
                                </div>
                            </div>
                        </div>

                        <div className={'footer'}>
                            <button>Подробнее</button>
                        </div>
                    </div>
                }) : <p>Empty Data!</p>
        }
    </div>
}

export default Results;