import React, {useState} from "react";
import Search from "./Search";
import FilterTypes from "./FilterTypes";
import MainSettings from "./FilterTypes/MainSettings";
import Categories from "./FilterTypes/Categories";
import Value from "./FilterTypes/Value";
import TradingDate from "./FilterTypes/TradingDate";
import SearchPatterns from "./FilterTypes/SearchPatterns";
import OtherParameters from "./FilterTypes/OtherParameters";

const Filter = () => {
    const [mainSettings, setMainSettings] = useState(false);
    const [categories, setCategories] = useState(false);
    const [otherParameters, setOtherParameters] = useState(false);
    const [values, setValues] = useState(false);
    const [tradingDate, setTradingDate] = useState(false);
    const [searchPatterns, setSearchPatterns] = useState(false);


    return <div className={'filter-container'}>
        <h2>Агрегатор электронных торгов по банкротству</h2>
        <Search />
        <FilterTypes
            mainSettings={mainSettings}
            setMainSettings={() => setMainSettings(!mainSettings)}
            categories={categories}
            setCategories={() => setCategories(!categories)}
            otherParameters={otherParameters}
            setOtherParameters={() => setOtherParameters(!otherParameters)}
            values={values}
            setValues={() => setValues(!values)}
            tradingDate={tradingDate}
            setTradingDate={() => setTradingDate(!tradingDate)}
            searchPatterns={searchPatterns}
            setSearchPatterns={() => setSearchPatterns(!searchPatterns)} />

        <div className={'w-100 d-flex flex-column align-items-center'}>
            {mainSettings && <MainSettings /> }
            {categories && <Categories /> }
            {otherParameters && <OtherParameters /> }
            {values && <Value /> }
            {tradingDate && <TradingDate /> }
            {searchPatterns && <SearchPatterns /> }
        </div>
    </div>
}

export default Filter;