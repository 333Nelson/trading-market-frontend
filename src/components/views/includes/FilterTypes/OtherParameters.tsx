import React from 'react'

const OtherParameters = () => {
    return <div className={'other-parameters-container'}>
        <h3>Другие Параметры</h3>
        
        <div>
            <label htmlFor="sort-by">Сортировка по</label>
            <select id="sort-by">
                <option selected value="1">Дате добавления</option>
                <option value="2">Ключевому слову</option>
                <option value="3">Цене</option>
                <option value="4">Дате начала торгов</option>
                <option value="5">Дате окончания торгов</option>
            </select>
        </div>
        <div>
            <label htmlFor="show-for-the-period">Показывать за период</label>
            <select id="show-for-the-period">
                <option selected value="1">Все</option>
                <option value="2">Час</option>
                <option value="3">День</option>
                <option value="4">Сутки</option>
                <option value="5">7 дней</option>
                <option value="6">30 дней</option>
            </select>
        </div>
        <div>
            <div>
                <input type="checkbox" id={'deleted'}/>
                <label htmlFor="deleted">Удалённые</label>
            </div>
            <div>
                <input type="checkbox" id={'completed'}/>
                <label htmlFor="completed">Завершённые</label>
            </div>
            <div>
                <input type="checkbox" id={'with-photo-only'}/>
                <label htmlFor="with-photo-only">Только с фото</label>
            </div>
            <div>
                <input type="checkbox" id={'organizers-reply-received-for-lot'}/>
                <label htmlFor="organizers-reply-received-for-lot">По лоту получен ответ организатора</label>
            </div>
        </div>
    </div>
}

export default OtherParameters;