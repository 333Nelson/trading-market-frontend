class Router {
    private _routeRedirect: Array<any> = [];
    private _routeList: Array<any> = [];
    private _routeLink: any = {};
    private _prefix = "";

    group(prefix: string, callback: Function) {
        const oldPrefix = this._prefix;
        this._prefix += prefix;

        callback();
        this._prefix = oldPrefix;
    }

    add(name: string, routeItem: any) {
        const route = routeItem;
        route.path = this._prefix + route.path;
        route.exact = true;

        this._routeList.push(route);
        this._routeLink[name] = route.path;
    }

    homePage(component: any, layout: any) {
        this.add("index", {
            path: "/",
            component,
            layout,
        });

        this.add("home", {
            path: "/home",
            component,
            layout,
        });
    }

    error404(component: any) {
        this.add("error", {
            path: "*",
            component: component,
            layout: null
        });
    }

    redirectRoute(from: string, name: string, args = {}) {
        from = this._prefix + from;
        this._routeRedirect.push({
            from,
            name,
            args,
        });
    }

    toUrl(name: string, args: any = {}) {
        let path = this._routeLink[name];
        for (let key in args) {
            const re = new RegExp(`\\:${key}`, "g");
            path = path.replace(re, args[key]);
        }

        return path;
    }

    info(name: any) {
        return this._routeList[name];
    }

    redirectList() {
        const list: Array<any> = [];
        this._routeRedirect.forEach((item) => {
            list.push({
                from: item.from,
                to: this.toUrl(item.name, item.args),
                exact: true,
            });
        });

        return list;
    }

    routeList() {
        return this._routeList;
    }
}

export default new Router();