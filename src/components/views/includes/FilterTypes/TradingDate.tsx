import React, {useState} from 'react';
import 'react-dates/initialize';
import {DateRangePicker} from "react-dates";
import 'react-dates/lib/css/_datepicker.css';

const TradingDate = () => {
    const DateRange: any = DateRangePicker
    const [startDate, setStartDate] = useState(null);
    const [endDate, setEndDate] = useState(null);
    const [focusedInput, setFocusedInput] = useState(null);

    return <div className={'trading-date-container'}>
        <h3>Дата торгов</h3>

        <div>
            <label htmlFor="start-of-trading">Начало торгов</label>
            <DateRange
                showDefaultInputIcon={true}
                keepFocusOnInput={true}
                keepOpenOnDateSelect={true}
                numberOfMonths={2}
                onDatesChange={date => {
                    setStartDate(date.startDate);
                    setEndDate(date.endDate);
                }}
                onFocusChange={input => setFocusedInput(input)}
                focusedInput={focusedInput}
                startDate={startDate}
                endDate={endDate}
                startDateId="startDate"
                endDateId="endDate"
            />
        </div>
    </div>
}

export default TradingDate;